from sys       import argv
from itertools import combinations

from Particle import White, Red, Green, Blue
from Colour import Colour

if 'tk' in argv: from     TkDisplay import     TkDisplay as Display
else           : from PygletDisplay import PygletDisplay as Display


class ABunchOfParticles:

    def __init__(self):
        self._particles = []

    def add(self, p):
        self._particles.append(p)

    def update(self, dt, display):
        limits = display.bounding_box
        for p in self._particles:
            p.move(dt)
            p.bounce(limits)

    def draw(self, display):
        for p in self._particles:
            display.draw_circle(p.r, p.x, p.y, p.colour)
        for p,q in combinations(self._particles, 2):
            if p.overlaps(q):
                p.collide(q)

model = ABunchOfParticles()

d = Display(model)
model.add(Blue (30, (200,200), (80,130)))
model.add(Red  (20, (200,200), (70,140)))
model.add(Green(15, (200,200), (60,150)))
model.add(Blue (50, (200,200), (10, 20)))
d.go()
