import tkinter as tk

from Display import Display

class TkDisplay(Display):

    def __init__(self, model):
        super().__init__(model)
        master = tk.Tk()
        self.w = tk.Canvas(master, width=600, height=400, bg='black')
        self.w.pack()

    def go(self):
        self.update(0)
        tk.mainloop()

    @property
    def bounding_box(self):
        return (0, self.w.winfo_width(), 0, self.w.winfo_height())

    def draw(self):
        self.w.delete(tk.ALL)
        self._model.draw(self)
        self.w.update()


    def update(self, dt):
        super().update(dt)
        self.draw()
        self.w.after(int(1000 * self._frame_rate), self.update, self._frame_rate)

    def draw_circle(self, r, x,y, c):
        self.w.create_oval(x-r, y-r, x+r, y+r, outline='#'+c.as_rgb_f())
