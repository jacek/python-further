from math   import sqrt
from random import randint

from abc import ABC, abstractmethod

from multimethod import multimethod

from Vector import Vector2D as Vector
from Colour import Colour


def normalize(vector):
    return vector / abs(vector)


class Particle:

    colour = Colour.WHITE

    def __init__(self, r, pos, v):
        self.r = r
        self._pos = Vector(*pos)
        self._vel = Vector(*v)

    @property
    def x(self):
        return self._pos.x
    @x.setter
    def x(self, new):
        self._pos.x = new

    @property
    def y(self):
        return self._pos.y
    @y.setter
    def y(self, new):
        self._pos.y = new

    @property
    def vx(self):
        return self._vel.x
    @vx.setter
    def vx(self, new):
        self._vel.x = new

    @property
    def vy(self):
        return self._vel.y
    @vy.setter
    def vy(self, new):
        self._vel.y = new

    @property
    def D(self):
        return self.r * 2

    def draw(self, display):
        display.draw_circle(self.r, self.x, self.y, self.colour)

    def move(self, dt):
        self._pos += dt * self._vel

    def bounce(self, box):
        xmin, xmax, ymin, ymax = box

        excess = xmin - (self.x - self.r)
        if excess > 0:
            self.x += 2 * excess
            self.vx = - self.vx

        excess = (self.x + self.r) - xmax
        if excess > 0:
            self.x -= 2 * excess
            self.vx = - self.vx

        excess = ymin - (self.y - self.r)
        if excess > 0:
            self.y += 2 * excess
            self.vy = - self.vy

        excess = (self.y + self.r) - ymax
        if excess > 0:
            self.y -= 2 * excess
            self.vy = - self.vy

    def overlaps(self, other):
        return (self.r - other.r) > abs(self._pos - other._pos)

    def collide(self, other):
        return collide(self, other, False)

class Cycling(Particle, ABC):

    @abstractmethod
    def turn(self): pass

    def move(self, dt):
        super().move(dt)
        self._vel += dt * self.turn()
        self._vel /= sqrt(1 + dt*dt)


class Green(Cycling):

    colour = Colour.GREEN

    def turn(self):
        return Vector(self.vy, -self.vx)


class Red(Cycling):

    colour = Colour.RED

    def turn(self):
        return Vector(-self.vy, self.vx)


class Oscillating(Particle, ABC):

    @abstractmethod
    def _check_limit(self): pass


    def move(self, dt):
        super().move(dt)
        self._vel *= self._delta_v
        self.r    /= self._delta_v
        self._check_limit()


class Blue(Oscillating):

    colour = Colour.BLUE
    _delta_v = 1.02

    def _check_limit(self):
        if abs(self._vel) > 500:
            self.__class__ = Yellow


class Yellow(Oscillating):

    colour = Colour.YELLOW
    _delta_v = 1 / Blue._delta_v

    def _check_limit(self):
        if abs(self.r) > 30:
            self.__class__ = Blue


class White(Particle):

    _decay_types = [(Red, Green), (Blue, Yellow)]

    def absorb(self, other):
        self._absorbed = other
        a , b  = self, other
        ma, mb = a.r ** 2, b.r ** 2
        M = ma + mb
        momentum = ma * a._vel + mb * b._vel
        self._original_rs = a.r, b.r
        self._original_vs = a._vel, b._vel
        self.r = sqrt(M)
        self._vel = momentum / M
        self._pos = (a._pos * ma + b._pos * mb) / M

    def move(self, dt):
        super().move(dt)
        self._absorbed._pos = self._pos
        if randint(0,100) == 0:
            self._decay()

    def _decay(self):
        a,b = self, self._absorbed
        a.r, b.r = self._original_rs
        a._vel, b._vel = self._original_vs
        # Ensure that decay products don't collide with eachother
        # immediately
        direction_of_separation = normalize(a._vel - b._vel)
        b._pos = self._pos - b.r * direction_of_separation * 1.001
        a._pos = self._pos + a.r * direction_of_separation * 1.001
        # Decay
        A,B = self._decay_types[randint(0,1)]
        if randint(0,1): A,B = B,A
        a.__class__ = A
        b.__class__ = B


class Absorbed(Particle):

    colour = Colour.from_rgb_01(0.2, 0.2, 0.2)

    def move(self, dt): pass


def centre_of_mass(*particles):
    return (sum((p.m * p._pos for p in particles), Vector(0,0)) /
            sum( p.m       for p in particles))

def centre_of_momentum(*particles):
    return (sum((p.m * p._vel for p in particles), Vector(0,0)) /
            sum( p.m       for p in particles))



@multimethod
def collide(a, b, args_already_reversed):
    if not args_already_reversed:
        collide(b, a, True)

# White does not interact at all, it only decays into two coloured
# particles.

# When particles of different types collide they combine to form a
# White one.
def combine(w,a):
    if a.r > w.r: w,a = a,w
    w.__class__ = White
    a.__class__ = Absorbed
    w.absorb(a)

@multimethod
def collide(r:Red   , g:Green , _): combine(r,g)
@multimethod
def collide(r:Red   , b:Blue  , _): combine(r,b)
@multimethod
def collide(r:Red   , y:Yellow, _): combine(r,y)
@multimethod
def collide(g:Green , b:Blue  , _): combine(g,b)
@multimethod
def collide(g:Green , y:Yellow, _): combine(g,y)
@multimethod
def collide(b:Blue  , y:Yellow, _): combine(b,y)
